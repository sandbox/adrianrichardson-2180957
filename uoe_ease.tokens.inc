<?php

/**
 * @file
 * Builds placeholder replacement tokens for user-related data.
 */

/**
 * Implements hook_token_info().
 */
function uoe_ease_token_info() {
  $user['uun'] = array(
    'name' => t("University User Name"),
    'description' => t("The user's EASE UUN."),
  );

  return array(
    'tokens' => array('user' => $user),
  );
}

/**
 * Implements hook_tokens().
 */
function uoe_ease_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'user' && !empty($data['user'])) {
    $account = $data['user'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Basic user account information - can't be overridden as in using format_username()
        case 'uun':
          $name = $account->name;
          $replacements[$original] = $sanitize ? check_plain($name) : $name;
          break;
      }
    }
  }

  if ($type == 'current-user') {
    $account = user_load($GLOBALS['user']->uid);
    $replacements += token_generate('user', $tokens, array('user' => $account), $options);
  }

  return $replacements;
}
